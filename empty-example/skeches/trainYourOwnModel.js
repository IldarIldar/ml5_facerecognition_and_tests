let winWidth=800
let winHeight=800


//The process of train our own neutal network consist in three principal parts
//1-Data Collection     2-Train model     3-Deploy   4-SAVE MODEL

let model
let state='collection'

function setup() {
  createCanvas(winWidth,winHeight)
  background(240)
  //configuracion de la red neuronal,  more info visit: https://learn.ml5js.org/#/reference/neural-network?id=neuralnetwork
  let options={
    inputs:['x','z'],
    outputs:['label'],
    task:'classification',
    debug:'true'
  }
  model=ml5.neuralNetwork(options)
  model.loadData('savedata.json') //cargamos la data
}

//1-DATA COLLECTION
//DIBUJAMOS UN CIRCULO CON UNA LETRA EN EL BACKGROUND AL CLICKAR, sera nuestro set de datos
function mousePressed(){

  let inputs={ //los inputs del modelo
    x:mouseX,
    y:mouseY
  }

  if(state=='collection'){
    let target={ //el output
      label:targetLabel
    }
    model.addData(inputs,target) //le añadimos la data
 
    stroke(0)
    noFill()
    ellipse(mouseX,mouseY,24)
    textAlign(CENTER,CENTER)
    text(targetLabel,mouseX,mouseY)  
  }else if(state=='deploy'){
      model.classify(inputs,gotResults)
  }
}

function keyPressed(){
    if(key=='t'){ //2-TRAIN THE MODEL
      state='training'
      model.normalizeData()//normalize data before train
      let options={
        epochs:200 //un epoch es un 'paquete' de datos escogidos aleatoriamente que se envian al modelo
                   //para entrenarlo, esto mandaria 100 epocs
      }
      model.train(options, whileTraining,finishedTraining) //whileTraining and finishedTraining son callbacks
    }else if(key=='s'){
        model.saveData('savedata')
    }else{
      targetLabel=key.toUpperCase()
    }
  }

  //very useful callback to do debuging
  function whileTraining(epoch,loss){
    console.log('epoch: '+epoch)

  }

  function finishedTraining(){
    console.log('finished training :D')
    state='deploy'
  }


//3-DEPLOY  
function gotResults(error,results){
  if(error){
    console.error(error)
    return
  }
  console.log(results)

  stroke(0)
  fill(0,200,150)
  ellipse(mouseX,mouseY,24)
  textAlign(CENTER,CENTER)
  text(results[0].label,mouseX,mouseY) 
} 

function draw() {


}