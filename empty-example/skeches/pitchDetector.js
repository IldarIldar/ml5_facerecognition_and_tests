let winWidth=800
let winHeight=800

const model_url ='https://cdn.jsdelivr.net/gh/ml5js/ml5-data-and-models/models/pitch-detection/crepe/'
let pitch
let mic
 
var pitchStart=false
var modelLoad=false
var freq=0

function setup() {
  createCanvas(winWidth,winHeight)
  background(240)

  audioContext=getAudioContext()
  mic=new p5.AudioIn()
  mic.start(listening)
}

function listening(){

  pitch = ml5.pitchDetection(model_url, audioContext, mic.stream, modelLoaded);
  pitchStart=true
}


function modelLoaded(){
  modelLoad=true
}


function gotPitch(error, frequency){
  if(!error){
    freq=frequency
  }else{
    console.error(error)
  }
}


function draw() {
  //console.log(freq)
  var a =Math.round(freq)
  background(250,200,100)

 

  if(modelLoad){
    if(pitchStart){
      pitch.getPitch(gotPitch)
    } 

    for (var i = 0; i < 10; i++){
      if(i%5==0){
        fill(random(a+100),random(a+100),random(a+100))
      }else{
        noFill()
      }
      ellipse(a*random(i/8),a*random(i/8),random(a))
    }
    
  }
}