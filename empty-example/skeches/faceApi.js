let faceapi;
let detections = [];

let poseNet;
let pose;
let skeleton;

let video;
let canvas;

var w=680
var h=460

var count=0

let ready=false

function setup() {
    frameRate(1);
  canvas = createCanvas(w, h);
  canvas.id("canvas");

  video = createCapture(VIDEO);// Creat the video: ビデオオブジェクトを作る
  video.id("video");
  video.size(width, height);

  //poseNet = ml5.poseNet(video);
  //poseNet.on('pose', gotPoses);

  const faceOptions = {
    withLandmarks: true,
    withExpressions: true,
    withDescriptors: true,
    minConfidence: 0.5
  };

  //Initialize the model: モデルの初期化
  faceapi = ml5.faceApi(video, faceOptions, faceReady);
}


function draw(){



    if(ready){
        faceapi.detect(gotFaces);// Start detecting faces: 顔認識開始

    }
   /* if (false) {
        let eyeR = pose.rightEye;
        let eyeL = pose.leftEye;
        let d = dist(eyeR.x, eyeR.y, eyeL.x, eyeL.y);
            
        for (let i = 0; i < pose.keypoints.length; i++) {
          let x = pose.keypoints[i].position.x;
          let y = pose.keypoints[i].position.y;
        }
    
        for (let i = 0; i < skeleton.length; i++) {
          let a = skeleton[i][0];
          let b = skeleton[i][1];
          stroke(random(44), random(169), 225);
          line(a.position.x, a.position.y, b.position.x, b.position.y);
        }
      }
*/

      
      for (let i = 0; i < 200; i++) {
        point(i*20,count*10*sin(i))
      }

      count++;

      if(count>h){
          count=0
      }
}

/*
function gotPoses(poses) {
    if (poses.length > 0) {
      pose = poses[0].pose;
      skeleton = poses[0].skeleton;
    }
  }

function gotPoses(poses) {
    if (poses.length > 0) {
      pose = poses[0].pose;
      skeleton = poses[0].skeleton;
    }
  }
*/
function faceReady() {
    ready=true
}

// Got faces: 顔を検知
function gotFaces(error, result) {
  if (error) {
    console.log(error);
    return;
  }

  detections = result;　//Now all the data in this detections: 全ての検知されたデータがこのdetectionの中に
  // console.log(detections);

  //clear();//Draw transparent background;: 透明の背景を描く
  //drawBoxs(detections);//Draw detection box: 顔の周りの四角の描画
   drawLandmarks(detections);//// Draw all the face points: 全ての顔のポイントの描画

  faceapi.detect(gotFaces);// Call the function again at here: 認識実行の関数をここでまた呼び出す
}

function drawBoxs(detections){
  if (detections.length > 0) {//If at least 1 face is detected: もし1つ以上の顔が検知されていたら
    for (f=0; f < detections.length; f++){
      let {_x, _y, _width, _height} = detections[f].alignedRect._box;
      stroke(44, 169, 225);
      strokeWeight(1);
      //noFill();
      rect(_x, _y, _width, _height);
    }
  }
}

function drawLandmarks(detections){
    background(100-random(20),100-random(20),100-random(20),40)

  if (detections.length > 0) {//If at least 1 face is detected: もし1つ以上の顔が検知されていたら
    
    for (f=0; f < detections.length; f++){
      let points = detections[f].landmarks.positions;
      for (let i = 0; i < points.length; i++) {
        strokeWeight(2);
        stroke(random(44), random(169), 225);
        //point(points[i]._x, points[i]._y);
        if(i>1){
        line(points[i]._x, points[i]._y,points[i-1]._x, points[i-1]._y)
        noStroke()
        fill(random(10*i),random(20*i),random(20*i))
        ellipse(points[i]._x, points[i]._y,random(6))
        }
      }
    }
  }
}
