let mobilenet
let video
let label=''

function modelReady(){
  mobilenet.predict(gotResults)
}

function gotResults(error,results){
  if(!error){
     label =results[0].label
    mobilenet.predict(gotResults)
  }
}

function setup() {
  createCanvas(800,800)
  video=createCapture(VIDEO)
  video.hide()
  mobilenet=ml5.imageClassifier('MobileNet',video,modelReady)

}

function draw() {
  background(255)

  // put drawing code here  
  image(video,0,0)
  fill(0)
  textSize(64)
  text(label,10,height-100)


}